clear all ; close all ; clc ;

%% read data
[time, x_pos, y_pos] = readData('SDF2018a.txt');

time_diff = diff(time) ;

dt = time_diff(1) ;

%calculate velocities from measurement
x_vel_meas = diff(x_pos)./dt;
x_vel_meas(end+1) =x_vel_meas(end);

y_vel_meas = diff(y_pos)./dt;
y_vel_meas(end+1) =y_vel_meas(end);

v_meas = sqrt(x_vel_meas.^2 + y_vel_meas.^2);

%calculate standard deviation of velocities 
std_vel_x = std(x_vel_meas) ;
std_vel_y = std(y_vel_meas) ;

std_pos = 50 ;

var_x = std_pos^2 ;

var_y = std_pos^2 ;

acceleration = 0.005e-3 ; %m/s^2

velocity = 1e-2 ; % m/s

% x_vel = velocity;
% y_vel = velocity;

ds = 0.5*acceleration*dt^2 ;

dv = acceleration*dt ;

%% set up
% state transition matrix , F
F = [1 0 dt 0 ; 0 1 0 dt ; 0 0 1 0 ; 0 0 0 1] ;

% measurement model
H = [1 0 0 0 ; 0 1 0 0; 0 0 1 0; 0 0 0 1] ;

% measurement error covariance
R = [var_x 0 0 0; 0 var_y 0 0; 0 0 std_vel_x^2 0; 0 0 0 std_vel_y^2 ] ;

%propagation error covariance
Q = [ds^2 0 0 0; 0 ds^2 0 0 ; 0 0 dv^2 0; 0 0 0 dv^2];

% initial state vector (location and velocity)
x = [x_pos(1) ; y_pos(1); x_vel_meas(1); y_vel_meas(1)] ; 

% initial state vector covariance (doubtful initialisation)
P = [var_x 0 0 0; 0 var_y 0 0; 0 0 std_vel_x^2 0; 0 0 0 std_vel_x^2] ;  

%measurements - read file 
measurements = [x_pos y_pos x_vel_meas y_vel_meas] ;

%% calculation
predicted_path = zeros(length(measurements(:,1)), 2) ;

for i = 1:length(measurements(:,1))
    
    % get measurement
    z = measurements(i,:)';
    
    %correct prediction by measurement(update initialization)
    [P,K,H,x] = measurementUpdate(z,x,H,R,P);
    
    %state prediction
    x = predictState(x,F) ;
    
    %predict state vector covariance
    P = predictCovariance(F,P,Q);
    
    predicted_path(i,:) = x(1:2)' ;
    
end

x_predicted = predicted_path(:,1) ;
y_predicted = predicted_path(:,2) ;

figure(1)
plot(x_pos,y_pos, 'color', 'r')
title('Measured vs predicted coordinates of the rover')
xlabel('X coordinate [m]')
ylabel('Y coordinate [m]')

hold on 
plot(x_predicted ,y_predicted , 'color', 'b')
legend('Measured','Predicted')


figure(10)
subplot(3,1,1)
plot(x_vel_meas);
title('Measured V_{x}')

subplot(3,1,2)
plot(y_vel_meas);
title('Measured V_{y}')
subplot(3,1,3)
plot(v_meas);
title('Measured V')