clear all ; close all ; clc ;

%AUTHOR : MOHD BILAL (s280205)

addpath(genpath('C:\Users\Bilal\Documents\MATLAB\MATLAB-Online-master\plotly'))

%% read data
[time, x_pos, y_pos, range] = readData('SDF2018b.txt');

beacon_loc = [0 0];

time_diff = diff(time) ;

dt = time_diff(1) ;

std_pos = 50 ;

var_x = std_pos^2 ;

var_y = std_pos^2 ;

var_r = std_pos^2 ;

acceleration = 0.005e-3 ; %m/s^2

velocity = 1e-2 ; % m/s

ds = 0.5*acceleration*dt^2 ;

dv = acceleration*dt ;


%% set up
% state transition matrix , F
F = [1 0 dt 0 ; 0 1 0 dt ; 0 0 1 0 ; 0 0 0 1] ;

% measurement error covariance
R = [var_x 0 0; 0 var_y 0; 0 0 var_r] ;

%propagation error covariance
Q = [ds^2 0 0 0; 0 ds^2 0 0 ; 0 0 dv^2 0; 0 0 0 dv^2];

% initial state vector (location and velocity)
x = [x_pos(1) ; y_pos(1); velocity; velocity] ;

% x = [x_pos(1) ; y_pos(1); 0; 0] ; 
% x = [0;0;0;0] ;
% initial state vector covariance (doubtful initialisation)
P = [var_x 0 0 0; 0 var_y 0 0; 0 0 velocity^2 0; 0 0 0 velocity^2] ;  

%measurements - read file 
measurements = [x_pos y_pos range] ;

%% calculation
predicted_path = zeros(length(measurements(:,1)), 2) ;
corrected_path = zeros(length(measurements(:,1)), 2) ;
corrected_vel  = zeros(length(measurements(:,1)), 2) ;

for i = 1:length(measurements(:,1))
    
    % get measurement
    z = measurements(i,:)';
    
    r = sqrt(x(1)^2 + x(2)^2) ;
    
    %measurement model
    H = [1 0 0 0 ; 0 1 0 0; x(1)/r x(2)/r 0 0] ;

    %correct prediction by measurement(update initialization)
    [P,K,H,x] = measurementUpdateEKF(z,x,H,R,P);
    
    corrected_path(i,:) = x(1:2)' ;
    corrected_vel(i,:) = x(3:4)' ;
    
    %state prediction
    x = predictState(x,F) ;
    
    %predict state vector covariance
    P = predictCovariance(F,P,Q);
    
    predicted_path(i,:) = x(1:2)' ;
    
end


%% post processsing

x_predicted = predicted_path(:,1) ;
y_predicted = predicted_path(:,2) ;


x_corrected = corrected_path(:,1) ;
y_corrected = corrected_path(:,2) ;

x_vel_corrected = corrected_vel(:,1) ;
y_vel_corrected = corrected_vel(:,2) ;


range_predicted = zeros(length(predicted_path(:,1)),1) ;
range_corrected = zeros(length(predicted_path(:,1)),1) ;
vel_mag_corrected = zeros(length(predicted_path(:,1)),1) ;

for i_range = 1:length(predicted_path(:,1)) 
    range_predicted(i_range,1) = norm(predicted_path(i_range,:)) ;
    range_corrected(i_range,1) = norm(corrected_path(i_range,:)) ;
    vel_mag_corrected (i_range,1) = norm(corrected_vel(i_range,:)) ;
end
 
%% plotting 

% Measured v/s predicted locus
fig1 = figure(1);
plot(x_pos,y_pos)
title('Measured v/s Predicted locus of the rover')
xlabel('X coordinate [m]')
ylabel('Y coordinate [m]')
grid on
grid minor

hold on 
plot(x_predicted ,y_predicted)
legend('Measured locus', 'Predicted locus', 'Location','northwest')
plotly_fig = fig2plotly(fig1, 'offline', true, 'filename','EKF Locus Predicted' );
plotly_fig.url

saveas(fig1,'EKFpredicted_locus','epsc')


% Measured v/s corrected locus
fig2 = figure(2);
plot(x_pos,y_pos)
title('Measured v/s Estimated locus of the rover')
xlabel('X coordinate [m]')
ylabel('Y coordinate [m]')
grid on
grid minor

hold on 
plot(x_corrected ,y_corrected)
legend('Measured locus', 'Estimated locus','Location','northwest')
plotly_fig = fig2plotly(fig2, 'offline', true, 'filename','EKF Locus Corrected' );
plotly_fig.url

saveas(fig2,'EKFestimated_locus','epsc')



%%
% range predicted
fig3 = figure(3);
plot(time,range)
title('Measured vs predicted range of the rover')
xlabel('time')
ylabel('range (m)')
grid on
grid minor

hold on 
plot(time,range_predicted)
legend('Measured range', 'Predicted range','Location','northwest')
plotly_fig = fig2plotly(fig3, 'offline', true, 'filename','EKF Range Predicted');
plotly_fig.url

saveas(fig3,'EKFpredicted_range','epsc')



%range corrected
fig4 = figure(4);
plot(time,range)
title('Measured vs Estimated range of the rover')
xlabel('time')
ylabel('range (m)')
grid on
grid minor

hold on 
plot(time,range_corrected)
legend('Measured range', 'Estimated range','Location','northwest')
plotly_fig = fig2plotly(fig4, 'offline', true, 'filename','EKF Range Corrected');
plotly_fig.url

saveas(fig4,'EKFestimated_range','epsc')

%%
%individual coordinates - X

fig5 = figure(5);
subplot(2,1,1)    
plot(time,x_pos, 'LineWidth',1.2)
title('Measured vs Estimated coordinates - X')
xlabel('time')
ylabel('X coordinates (m)')
grid on
grid minor

hold on 
plot(time,x_corrected,'LineWidth',1.2)

legend('Measured coordinates x', 'Estimated coordinates x')


%individual coordinates - Y

subplot(2,1,2)    
plot(time,y_pos,'LineWidth',1.2)
title('Measured vs Estimated coordinates - Y')
% xlabel('time')
ylabel('Y coordinates (m)')
grid on
grid minor

hold on 
plot(time,y_corrected,'LineWidth',1.2)

legend('Measured coordinates y', 'Estimated coordinates y')
plotly_fig = fig2plotly(fig5, 'offline', true, 'filename','EKF Coordinates Corrected - XY');
plotly_fig.url

saveas(fig5,'EKFestimated_coord','epsc')

%%  velocities

fig6 = figure(6);

subplot(3,1,1)    
plot(time,x_vel_corrected,'LineWidth',1.2)
title('Estimated velocities - V_{x}')
xlabel('time')
ylabel('Velocity_x (m/s)')
grid on
grid minor


subplot(3,1,2)    
plot(time,y_vel_corrected,'LineWidth',1.2)
title('Estimated velocities - V_{y}')
xlabel('time')
ylabel('Velocity_y (m/s)')
grid on
grid minor

subplot(3,1,3)    
plot(time,vel_mag_corrected,'LineWidth',1.2)
title('Estimated speed - V')
xlabel('time')
ylabel('Velocity (m/s)')
grid on
grid minor

plotly_fig = fig2plotly(fig6, 'offline', true, 'filename','EKF Coordinates Corrected - XY');
plotly_fig.url

saveas(fig6,'EKFestimated_vel','epsc')
