clear all ; close all ; clc ;

%AUTHOR : MOHD BILAL (s280205)

%% read data
[time, x_pos, y_pos] = readData('SDF2018a.txt');

time_diff = diff(time) ;

dt = time_diff(1) ;

std_pos = 50 ;

var_x = std_pos^2 ;

var_y = std_pos^2 ;

acceleration = 0.005e-3 ; %m/s^2

velocity = 1e-2 ; % m/s

ds = 0.5*acceleration*dt^2 ;

dv = acceleration*dt ;

%% set up
% state transition matrix , F
F = [1 0 dt 0 ; 0 1 0 dt ; 0 0 1 0 ; 0 0 0 1] ;

% measurement model
H = [1 0 0 0 ; 0 1 0 0] ;

% measurement error covariance
R = [var_x 0 ; 0 var_y] ;

%propagation error covariance
Q = [ds^2 0 0 0; 0 ds^2 0 0 ; 0 0 dv^2 0; 0 0 0 dv^2];

fname1 = 'KFpredicted_locusQsmall' ;
fname2 = 'KFestimated_locusQsmall' ;
fname3 = 'KFestimated_coordQsmall' ;
fname4 = 'KFestimated_velQsmall'   ;


% initial state vector (location and velocity)
% x = [x_pos(1) ; y_pos(1); 0; 0] ; 
x = [x_pos(1) ; y_pos(1); velocity; velocity] ;

% initial state vector covariance (doubtful initialisation)
P = [std_pos^2 0 0 0; 0 std_pos^2 0 0; 0 0 velocity^2 0; 0 0 0 velocity^2] ;  

%measurements - read file 
measurements = [x_pos y_pos] ;

%% calculation
predicted_path = zeros(length(measurements(:,1)), 2) ;
predicted_vel = zeros(length(measurements(:,1)), 2) ;

corrected_path = zeros(length(measurements(:,1)), 2) ;
corrected_vel  = zeros(length(measurements(:,1)), 2) ;

for i = 1:length(measurements(:,1))
    
    % get measurement
    z = measurements(i,:)';
    
    %correct prediction by measurement(update initialization)
    [P,K,H,x] = measurementUpdate(z,x,H,R,P);
    
    %record data
    corrected_path(i,:) = x(1:2)' ;
    corrected_vel(i,:) = x(3:4)' ;
    
    %state prediction
    x = predictState(x,F) ;
    
    %predict state vector covariance
    P = predictCovariance(F,P,Q);
    
    predicted_path(i,:) = x(1:2)' ;
    predicted_vel(i,:) = x(3:4)' ;
    
end

%% post processing

x_predicted = predicted_path(:,1) ;
y_predicted = predicted_path(:,2) ;

x_corrected = corrected_path(:,1) ;
y_corrected = corrected_path(:,2) ;

x_vel_corrected = corrected_vel(:,1) ;
y_vel_corrected = corrected_vel(:,2) ;

vel_mag_corrected = zeros(length(predicted_path(:,1)),1) ;

for i_range = 1:length(predicted_path(:,1)) 
    vel_mag_corrected (i_range,1) = norm(corrected_vel(i_range,:)) ;
end

%% plotting


fig1 = figure(1);
plot(x_pos,y_pos)
title('Measured v/s Predicted locus of the rover - 0.1*Q')
xlabel('X coordinate [m]')
ylabel('Y coordinate [m]')
grid on
grid minor

hold on 
plot(x_predicted ,y_predicted)
legend('Measured locus', 'Predicted locus', 'Location','northwest')
plotly_fig = fig2plotly(fig1, 'offline', true, 'filename','KF Locus Predicted' );
plotly_fig.url

saveas(fig1,fname1,'epsc')


% Measured v/s corrected locus
fig2 = figure(2);
plot(x_pos,y_pos)
title('Measured v/s Estimated locus of the rover - 0.1*Q')
xlabel('X coordinate [m]')
ylabel('Y coordinate [m]')
grid on
grid minor

hold on 
plot(x_corrected ,y_corrected)
legend('Measured locus', 'Estimated locus','Location','northwest')
plotly_fig = fig2plotly(fig2, 'offline', true, 'filename','KF Locus Corrected' );
plotly_fig.url

saveas(fig2,fname2,'epsc')

%%

%individual coordinates - X

fig5 = figure(5);
subplot(2,1,1)    
plot(time,x_pos, 'LineWidth',1.2)
title('Measured vs Estimated coordinates - X, - 0.1*Q')
xlabel('time')
ylabel('X coordinates (m)')
grid on
grid minor

hold on 
plot(time,x_corrected,'LineWidth',1.2)

legend('Measured coordinates x', 'Estimated coordinates x')


%individual coordinates - Y

subplot(2,1,2)    
plot(time,y_pos,'LineWidth',1.2)
title('Measured vs Estimated coordinates - Y - 0.1*Q')
% xlabel('time')
ylabel('Y coordinates (m)')
grid on
grid minor

hold on 
plot(time,y_corrected,'LineWidth',1.2)

legend('Measured coordinates y', 'Estimated coordinates y')
plotly_fig = fig2plotly(fig5, 'offline', true, 'filename','KF Coordinates Corrected - XY');
plotly_fig.url

saveas(fig5,fname3,'epsc')

%%
fig6 = figure(6);

subplot(3,1,1)    
plot(time,x_vel_corrected,'LineWidth',1.2)
title('Estimated velocities - V_{x} - 0.1*Q')
xlabel('time')
ylabel('Velocity_x (m/s)')
grid on
grid minor


subplot(3,1,2)    
plot(time,y_vel_corrected,'LineWidth',1.2)
title('Estimated velocities - V_{y} - 0.1*Q')
xlabel('time')
ylabel('Velocity_y (m/s)')
grid on
grid minor

subplot(3,1,3)    
plot(time,vel_mag_corrected,'LineWidth',1.2)
title('Estimated speed - V - 0.1*Q')
xlabel('time')
ylabel('Velocity (m/s)')
grid on
grid minor

plotly_fig = fig2plotly(fig6, 'offline', true, 'filename','KF Coordinates Corrected - XY');
plotly_fig.url

saveas(fig6,fname4,'epsc')


figure(7)
plot(predicted_vel)

