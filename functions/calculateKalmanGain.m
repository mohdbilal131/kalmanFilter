function K = calculateKalmanGain(H,R,P)

% Inputs
% H : measurement model
% R : measurement error covariance matrix
% P : predicted state vector covariance


% Outputs
% K : kalman gain


S = H*P*H' +  R ;
K = (P*H')/S ;


end

