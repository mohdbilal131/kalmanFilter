function [t,x_pos,y_pos,range] = readData(filename)

data=dlmread(filename,' ',3,0);

t =     data(:,1) ;
x_pos = data(:,2) ;
y_pos = data(:,3) ;

if nargout > 3
    range = data(:,4);
end

close all ;
end

