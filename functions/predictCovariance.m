function P_new = predictCovariance(F,P,Q)
% Inputs 
% F : state transition matrix
% P : previous state vector covariance (error on the state vector)
% Q : propagation error covariance (error on the propagation)

% Outputs 
% P_new : predicted state vector covariance

P_new = F*P*F' + Q ; 

disp('Predicted covariance: ');
disp(P_new)

end

