function [P_mupdate,K,H,x] = measurementUpdate(z,x,H,R,P)
% Inputs
% z : measurement
% x : state vector
% P : state vector covariance (error on the state vector)
% R : measurement error covariance matrix
% H : measurement model
% Outputs
% P_mupdate : state vector covariance
I = eye(length(x)) ;
y = z - H*x ;
K = calculateKalmanGain(H,R,P) ;
x = x + K*y ;
P_mupdate = (I-K*H)*P ;

% disp('Current/Measured state estimate: ');
% disp(x)
% disp('Current/Measured covariance: ');
% disp(P_mupdate)

end

