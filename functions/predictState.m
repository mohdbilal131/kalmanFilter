function state = predictState(x,F)
% Inputs 
% x : current state vector
% F : state transition matrix

%Outputs 
%state : predicted state

state = F*x ;

disp('Predicted state estimate: ');
disp(state)


end